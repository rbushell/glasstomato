Glass Tomato
============  
  
A simple pomodoro timer for Glass.  Say "ok glass, do some work" to get going.  

With the timer running,   
  select 'Stop' to stop the timer  
  select 'Turbo Mode' to speed things up  
   

