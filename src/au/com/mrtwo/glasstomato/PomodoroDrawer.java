package au.com.mrtwo.glasstomato;

import java.util.concurrent.TimeUnit;

import com.google.android.glass.media.Sounds;

import android.content.Context;
import android.graphics.Canvas;
import android.media.AudioManager;


import android.util.Log;
import android.view.SurfaceHolder;
import android.view.View;


public class PomodoroDrawer implements SurfaceHolder.Callback {


    private static final int mFinishSoundId = Sounds.SUCCESS;
    private static final int mTickSoundId = Sounds.TAP;
    
    private final PomodoroView mWorkingView;
    private final PomodoroBreakView mBreakView;
    private PomodoroView mCurrentView;
    
    private final Context mAppContext;
    
    private SurfaceHolder mHolder;
	private Pomodoro mPomodoro;
	private boolean mTick;       // Should we play a tic-toc sound?
	
    public PomodoroDrawer(Context context, Pomodoro pom) {
 
    	mAppContext = context.getApplicationContext();
        mWorkingView = new PomodoroWorkView(context);
        mBreakView = new PomodoroBreakView(context);
        mCurrentView = mWorkingView;
        
        mPomodoro = pom;
        mPomodoro.setListener(mPomodoroListener);
    }

    public void enableTicking ()
    {
    	mTick = true;
    }
    
	@Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        // Measure and layout the view with the canvas dimensions.
        int measuredWidth = View.MeasureSpec.makeMeasureSpec(width, View.MeasureSpec.EXACTLY);
        int measuredHeight = View.MeasureSpec.makeMeasureSpec(height, View.MeasureSpec.EXACTLY);

        mWorkingView.measure(measuredWidth, measuredHeight);
        mWorkingView.layout(
                0, 0, mWorkingView.getMeasuredWidth(), mWorkingView.getMeasuredHeight());
        mBreakView.measure(measuredWidth, measuredHeight);
        mBreakView.layout(
                0, 0, mBreakView.getMeasuredWidth(), mBreakView.getMeasuredHeight());
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        mHolder = holder;
        mPomodoro.start();

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        mPomodoro.stop();
        mHolder = null;
    }

    /**
     * Draws the view in the SurfaceHolder's canvas.
     */
    private void draw() {
        Canvas canvas;
        try {
            canvas = mHolder.lockCanvas();
        } catch (Exception e) {
            return;
        }
        if (canvas != null) {
            mCurrentView.draw(canvas);
            mHolder.unlockCanvasAndPost(canvas);
        }
    }
    
    private Pomodoro.Listener mPomodoroListener = new Pomodoro.Listener() {
        @Override
        public void onTick() {
        	Log.d("TOMATO", "ticking");
        	updateTime();
        	maybeTick();
        	draw();
        }
        
        public void onFinish( boolean working) {
            	play(mAppContext, mFinishSoundId);

            	if ( working ) {
            		// Not done yet, switch to the 5 minute break view
            		enableBreakView();
            	}
        		Log.d("TOMATO", "finished");
        }
        
        public void onTurboMode ( boolean on )
        {
        	mTick = on;
        }
    };
    
	private void updateTime( ) {
		long seconds = mPomodoro.getTimeRemaining();
		Log.d("TOMATO", String.format("remaining: %d", seconds));
		mCurrentView.setTime(TimeUnit.SECONDS.toMinutes(seconds), seconds % 60);				
	}
    
    private void enableBreakView() {
    	mCurrentView = mBreakView;
	}


	protected void play(Context context, int soundId) {
        AudioManager audio = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        audio.playSoundEffect(soundId);
	}
	
    
	protected void maybeTick() {
		if ( mTick ) {
			play ( mAppContext, mTickSoundId);
		}
	}

    

}
