package au.com.mrtwo.glasstomato;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.TextView;

public class PomodoroView extends FrameLayout {

	protected final TextView mMinuteView;
	protected final TextView mSecondView;

    public PomodoroView(Context context) {
        this(context, null, 0);
    }

    public PomodoroView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PomodoroView(Context context, AttributeSet attrs, int style) {
        this(context, attrs, style, R.layout.card_working);
    }
	
	public PomodoroView(Context context, AttributeSet attrs, int defStyle, int card) {
		super(context, attrs, defStyle);
        LayoutInflater.from(context).inflate(card, this);

        mMinuteView = (TextView) findViewById(R.id.minute);
        mSecondView = (TextView) findViewById(R.id.second);
	}

	public void setTime(long minutes, long seconds) {
	    mMinuteView.setText(String.format("%02d", minutes));
	    mSecondView.setText(String.format("%02d", seconds));
	}
}