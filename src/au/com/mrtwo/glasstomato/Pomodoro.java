package au.com.mrtwo.glasstomato;

import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;

public class Pomodoro {

	private static final int POMODORO_SECS = 25 * 60; // 25 minute intervals 
	private static final int BREAK_SECS =   5 * 60;    // 5 minute break
	private static final double TIME_DILATION_MULTIPLIER = 1.1;   // Turbo mode - time passes 10% faster

	boolean mRunning;
	boolean mStarted;
	boolean mWorking;
	
	boolean mTickSounds;
	long mStartTimeMillis;
	double mTimeDilation = 1.0;
	
	Listener mListener;

	Handler mHandler = new Handler();
	
	
    public interface Listener {
    	// once a second
    	public void onTick();
        // We have finished a pomodoro stage - working, then break
        public void onFinish( boolean isWorking);
        
        public void onTurboMode ( boolean enabled);
    }
	
    public boolean isWorking ()
    {
    	return mStarted && mWorking;
    }
    
    public boolean isBreak ()
    {
    	return mStarted && !mWorking;
    }
    
	public Runnable mTickRunnable = new Runnable() {
	
		@Override
	    public void run() {
	        if (mRunning) {
	        	// Wake me up in a sec
	        	Log.d("TOMATO", String.format("Waking up in %d",(long) (1000/mTimeDilation)));
	        	mHandler.postDelayed(mTickRunnable, (long) (1000/mTimeDilation));        
	        	if ( getTimeRemaining() < 0 ) {
	        		onFinishCurrentState();
	        	} else {
	        		if ( mListener != null) { 
	        				mListener.onTick();
	        		}
	        	}
	        	
    
	        }
	    }
	
	};
	
	public void setListener ( Listener l )
	{
		mListener = l;
	}
	
	protected void onFinishCurrentState() {
		if ( mListener != null ) {
			mListener.onFinish(mWorking);
		}
		if ( mWorking )
		{
			mWorking = false;
			updateRunning();
		} else {
			stop();
		}
	}

	public void start() {
	   	Log.d("TOMATO", "starting");
		mWorking = true;
        mStarted = true;
        mStartTimeMillis = SystemClock.elapsedRealtime();
        updateRunning();
	}
    
    void stop() {
        mStarted = false;
        updateRunning();
    }
    
	private void updateRunning() {
		boolean running = mStarted;
        if (running != mRunning) {
            if (running) {
            	Log.d ( "TOMATO", "posting");
                mHandler.post(mTickRunnable);
            } else {
            	Log.d ( "TOMATO", "removing");
                mHandler.removeCallbacks(mTickRunnable);
            }
            mRunning = running;
        }
    }


	// How long(in mS) has passed in the real world since we started this pomodoro?
	// May not match user time if turbo mode is on
	private long getRealTimeElapsed()
	{
		return SystemClock.elapsedRealtime() - mStartTimeMillis;
	}

	public long getTimeRemaining() {
		if ( mStarted ) {
			if ( mWorking) {
				return POMODORO_SECS - (long) (getRealTimeElapsed() * mTimeDilation)/1000;
			} else {
				return BREAK_SECS - (long) (getRealTimeElapsed() * mTimeDilation)/1000;
			}
		} else {
			return POMODORO_SECS;
		}
	}

	public void setTurboMode(boolean on) {
		if ( on ) {
			mTimeDilation = TIME_DILATION_MULTIPLIER;
		} else {
			mTimeDilation = 1;
		}
		updateRunning();
		if ( mListener != null ) {
				mListener.onTurboMode(on);
		}
	
		// TODO Auto-generated method stub
		
	}
}