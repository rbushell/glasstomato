package au.com.mrtwo.glasstomato;

import android.content.Context;
import android.util.AttributeSet;

public class PomodoroWorkView extends PomodoroView {


    public PomodoroWorkView(Context context) {
        this(context, null, 0);
    }

    public PomodoroWorkView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PomodoroWorkView(Context context, AttributeSet attrs, int style) {
        super(context, attrs, style, R.layout.card_working);

    }
}
