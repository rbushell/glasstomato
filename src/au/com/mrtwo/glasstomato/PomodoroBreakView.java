package au.com.mrtwo.glasstomato;

import android.content.Context;
import android.util.AttributeSet;

public class PomodoroBreakView extends PomodoroView {

    public PomodoroBreakView(Context context) {
        this(context, null, 0);
    }

    public PomodoroBreakView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PomodoroBreakView(Context context, AttributeSet attrs, int style) {
        super(context, attrs, style, R.layout.card_break);

    }

}
