package au.com.mrtwo.glasstomato;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import com.google.android.glass.timeline.LiveCard;
import com.google.android.glass.timeline.LiveCard.PublishMode;
import com.google.android.glass.timeline.TimelineManager;

public class GlassTomatoService extends Service {

    private static final String LIVE_CARD_TAG = "glasstomato";

    private PomodoroDrawer mDrawer;
    private LiveCard mLiveCard;
    private Pomodoro mPomodoro;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (mLiveCard == null) {
            mLiveCard = TimelineManager.from(this).createLiveCard(LIVE_CARD_TAG);

            mPomodoro = new Pomodoro();
            mDrawer = new PomodoroDrawer(this, mPomodoro);
            
            mLiveCard.setDirectRenderingEnabled(true).getSurfaceHolder().addCallback(mDrawer);

            Intent menuIntent = new Intent(this, MenuActivity.class);
            menuIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            mLiveCard.setAction(PendingIntent.getActivity(this, 0, menuIntent, 0));
            mLiveCard.publish(PublishMode.REVEAL);
            Log.d("TOMATO", "livecard published");
        }

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        if (mLiveCard != null && mLiveCard.isPublished()) {
            mPomodoro.stop();
        	if (mDrawer != null) {
                mLiveCard.getSurfaceHolder().removeCallback(mDrawer);
            }
            mLiveCard.unpublish();
            mLiveCard = null;
        }
        super.onDestroy();
    }

    public class ServiceBinder extends Binder {
    	Pomodoro getPomodoro() {
    		return mPomodoro;
    	}
    }
    
    @Override
    public IBinder onBind(Intent intent) {
        return new ServiceBinder();
    }
}
